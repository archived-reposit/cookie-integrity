const cookie = require('cookie');
const fs = require('fs');
const author = require('./author');
const dbService = require('./db-service');
//const express = require('express')
module.exports = {
    root: async (req, res) => { //get
        console.log('root-url')
        // res.send('Hello HTTPS!')

        // if !auth send login else squeaks
        const authorizedCookie = await author.authorizeSession(req)
        console.log('authorized session, cookie: ' + JSON.stringify(authorizedCookie))
        if (authorizedCookie) {
            console.log('authorized session, cookie: ' + authorizedCookie)
            // res.setHeader('Content-type', 'text/html');
            //res.send(true)
            sendSqueaks(res, authorizedCookie.username) //, authorizedCookie.sessionId)
        } else {
            sendLogin(res, false)
        }
    },
    // csrf: (req, res) => { // get
    //     console.log('csrf')
    //     res.setHeader('Content-type', 'text/html');
    //     //let data = fs.readFileSync('csrf.html').toString()
    //     res.sendFile('csrf.html');
    // },
    sign_in: async (req, res) => { // post
        //        req.app.use(express.urlencoded({ extended: false }))

        console.log('signin')
        // försök med callbacks, men res blir undef
        //author.login(req.body, (res) => { sendSqueaks(res) }, (res) => { sendLogin(res, false) })
        console.log('req.body :' + JSON.stringify(req.body))
        const authenticatedOrNot = await author.authenticateLogin(req.body)
        console.log('authenticatedOrNot :' + JSON.stringify(authenticatedOrNot))
        if (authenticatedOrNot.valid) {
            res = await setCookieHeader(res, authenticatedOrNot.username, authenticatedOrNot.sessionId, authenticatedOrNot.hash)
            //res.send(true)
            sendSqueaks(res, authenticatedOrNot.username) // code on client side checks answer, if truthy triggers page reload which should then somehow have the sessionid

        }
        else
            sendLogin(res, true)

    },
    sign_up: async (req, res) => {
        console.log('signup')
        //        const NewUserCredentialsValid = author.CheckIfNewUserCredentialsValid(req.body)
        const NewUserCredentialsValid = { nameValid: true, pwdValid: true }
        console.log('NewUserCredentialsValid ' + JSON.stringify(NewUserCredentialsValid))
        if (NewUserCredentialsValid.nameValid && NewUserCredentialsValid.pwdValid) {
            author.storeNewUser(req.body) // contains async code, but shouldn't matter since it should take enough time before logging in with credentials
            res = await setCookieHeader(res, req.body.username)
            // 
            res.send({ 'success': true })
        }
        else {
            res.send({ 'success': false, 'reason': NewUserCredentialsValid.nameValid ? 'password' : 'username' })
        }
    },
    sign_out: (req, res) => {
        console.log('signout')
        author.clearSession(req)
        // clear cookie
        res.setHeader('Set-Cookie', ['squeak-session=;Expires=' + ';expires=' + new Date(new Date().getTime() - 6000).toUTCString() + ';HttpOnly;', 'language = javascript']);
        res.send('Signing out!') // client checks if true and reloads

    },
    squeak: async (req, res) => {
        //console.log('squeak form: ' + JSON.stringify(req.body))
        // todo: authenticera
        const authorizedCookie = await author.authorizeSession(req)
        console.log('authorized session, cookie: ' + JSON.stringify(authorizedCookie))
        if (authorizedCookie) {
            //res.send('Squeak!')
            //        let squeaks = JSON.parse(fs.readFileSync('squeaks.json').toString())
            // let squeaks = JSON.parse(fs.readFileSync('squeaks.json').toString())
            // todo: get username from cookie
            // squeaks.push({ 'username': authorizedCookie.username, 'squeak': req.body.squeak, 'date': new Date().toDateString() })
            //console.log('squeak ' + JSON.stringify(squeaks[squeaks.length - 1]))
            // squeaks.squeak[req.body.user] = req.body.squeak
            // fs.writeFileSync('squeaks.json', JSON.stringify(squeaks))
            await dbService.writeSqueak({ 'username': authorizedCookie.username, 'squeak': req.body.squeak, 'date': new Date().toDateString(), 'recipient': req.body.recipient })
            sendSqueaks(res, authorizedCookie.username) //,
        } else
            sendLogin(res, false)
    }
}

//console.log('controller')

async function setCookieHeader(res, username, sessionId, hash) {
    console.log('setCookieHeader: ' + sessionId)
    if (!sessionId) // not sent when called from signing up
        // sessionId = author.wrapperAddSessionId() //old code, stores in file
        sessionId = await author.addSessionId()
    const cookieObject = { 'username': username, 'sessionid': sessionId, 'hash': hash }
    console.log('setCookieHeader: ' + JSON.stringify(cookieObject))
    // console.log('sessionId ' + sessionId)
    res.setHeader('Set-Cookie', ['squeak-session=' + JSON.stringify(cookieObject) + ';Expires=' + ';expires=' + expTime() + ';HttpOnly;', 'language = javascript']); // should add sessionId so that when refreshed on client..
    return res
}

function expTime() { // add some time before expiring + wintertime
    return new Date(new Date().getTime() + 60 * 60 * 1000 + 60 * 3 * 1000).toUTCString()
}

function sendLogin(res, rejected) {
    //console.log(res)
    console.log('send login, rejected? ' + rejected)
    // todo: add error message, not woring due to reload on client
    let errorMessage = ''
    if (rejected)
        errorMessage = '<div class="container"><div class="alert alert-danger" role="alert">Login failed. Invalid username or password.</div></div>'
    res.setHeader('Content-type', 'text/html');
    //    res.send(data);
    res.render('login', { title: 'Login', errorMessage: errorMessage })
}

async function sendSqueaks(res, username) {//, authorizedCookie) {
    console.log('send squeaks')
    // let data = fs.readFileSync('templates/squeak.html').toString()
    // //    res.statusCode = 302 // krävdes när man redirectade från servern tror jag? Nu reloadas från klienten
    // data = data.toString().replace('{{username}}', username)
    // data = data.toString().replace('{{squeaks}}', squeaksToHtml())
    res.setHeader('Content-type', 'text/html');
    //    res.setHeader('Location', "https://localhost:8000/");
    //res.setHeader('Set-Cookie', ['squeak-session=' + authorizedCookie + '; Expires = ' + '; expires = ' + new Date(new Date().getTime() - 6000).toUTCString(), 'language = javascript']);
    let squeaks = await dbService.readSqueaks(username);
    console.log(squeaks.length)
    //    console.log(squeals.length)
    let squeals = squeaks.filter(squeak => squeak.recipient == username)
    squeaks = squeaks.filter(squeak => squeak.recipient == 'all')
    console.log(squeaks.length)
    console.log(squeals.length)

    //console.log('username: sendsqueaks: ' + username)
    //fs.readFileSync('squeaks.json').toString()
    console.log('c squeaks : ' + JSON.stringify(squeaks))
    //squeaks = JSON.parse(squeaks)
    res.render('squeak', { 'username': username, 'squeakArr': squeaks, 'squealArr': squeals });
}

function squeaksToHtml() {
    let squeaks = readFromDB('squeaks'); //fs.readFileSync('squeaks.json').toString()
    squeaks = JSON.parse(squeaks) //array
    console.log('squeaks: ' + JSON.stringify(squeaks))
    let squeakHtml = ''
    squeaks.forEach((squeak) => {
        console.log(squeak.date)
        squeakHtml += '<div class="card mb-2"><div class="card-header">' + squeak.username + '<span class="float-right" >' + squeak.date + '</span></div>' + '<div class="card-body"><p class="card-text">' + squeak.squeak + '</p></div></div>'
    })
    return squeakHtml
}


// async function connectDB() {
//     mongoURL = "mongodb+srv://aD7:8cJGY6zfwfjJ@cluster0-oijer.mongodb.net/test?retryWrites=true" // todo: replace test ?
//     return MongoClient.connect(mongoURL)
//     // .then((cluster) => {
//     //     mongoCluster = cluster;

//     //        const db = cluster.db('Squeak!');
//     // squeaks = db.collection('squeaks');
//     // credentials = db.collection('credentials');
//     // sessions = db.collection('sessions');

//     //     console.log('connected to db? ')

//     //     // let server = https.createServer(options, app);
//     //     // server.listen(8000);
//     // }
//     //     )
//     //         .catch ((error) => {
//     //     console.log(error);
//     // }
//     //         );
// }

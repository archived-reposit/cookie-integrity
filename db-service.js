const fs = require('fs');
const MongoClient = require('mongodb').MongoClient;
var db = null;
const patched = false;
//const db_cred = JSON.parse(fs.readFileSync('cred.json').toString())
const user =  'yousir';
const pass = 'passwork';
//const db_cred = JSON.parse(fs.readFileSync('cred.json').toString()) // stange problem when read from file, string looks correct but is different..?
async function connect() {
    //const mongoURL = `mongodb+srv://${db_cred.user}:${db_cred.pwd}@cluster0-oijer.mongodb.net/test?retryWrites=true`;
    const mongoURL = `mongodb+srv://${user}:${pass}@cluster0-oijer.mongodb.net/test?retryWrites=true`; // todo: replace test ?
    console.log('mongoURL ' + mongoURL)
    return await MongoClient.connect(mongoURL, { useNewUrlParser: true })
        .then((cluster) => {
            //return cluster.db('Squeak!');
            db = cluster.db('Squeak!');
        })
        .catch((error) => {
            console.log(error);
        }
        );
}

function whiteListCharNumber(string) {
    if (!patched)
        return true
    return !string.match(/[^a-zA-Z0-9]/)
}

function whiteListHex(string) {
    if (!patched)
        return true
    return !string.match(/[^a-fA-F0-9]/)
}

function whiteListPassword(string) { // todo: store hash than no whitelisting should be neccesary for pw.
    if (!patched)
        return true
    // from https://www.owasp.org/index.php/Injection_Prevention_Cheat_Sheet_in_Java#Example_-_MongoDB
    return string.match(/[/\'\"\\\;\{\}\}\$]/) == null
}


exports.login = async function(username, password) {
    //let readDB = async function() {
    //JSON.parse(fs.readFileSync('squeaks.json').toString())
    console.log('login ' + username + ' ' + password)
    console.log(whiteListCharNumber(username))
    console.log(whiteListPassword(password))
    if (!whiteListCharNumber(username) || !whiteListPassword(password))
        return false
    if (db == null)
        await connect()
    let user = await db.collection('credentials').findOne({
        username: username,
        password: password
    });
    console.log('dbs: ' + user)
    return user !== null;
}

exports.addSessionId = async function(sessionId) {
    //    console.log('dbs, addsessionid received: ' + sessionId)
    if (!whiteListHex(sessionId))
        return false

    if (db == null)
        await connect()
    let res = await db.collection('sessions').insertOne({ id: sessionId });
    return res
}

exports.checkSessionId = async function(sessionId) {
    //console.log('checksessionid, sent id: ' + JSON.stringify(sessionId))
    if (db == null)
        await connect()
    let res = await db.collection('sessions').findOne({ id: sessionId });
    console.log('checksessionid: ' + res)
    return res
}

exports.invalidateSession = async (sessionId) => {
    return await db.collection('sessions').findOneAndDelete({ id: sessionId });
}

exports.writeNewUser = async function(username, password) {
    // todo: check user does not exist
    db.collection('credentials').insertOne({ username: username, password: password })
}

exports.checkIfUserNameAvailable = async function(username) {
    if (db == null)
        await connect()
    let user = await db.collection('credentials').findOne({
        username: username
    });
    console.log('dbs, checkIfUserNameAvailable: ' + user)
    return user == null;
}


exports.readSqueaks = async function(username) {
    //let readDB = async function() {
    //JSON.parse(fs.readFileSync('squeaks.json').toString())
    if (db == null)
        await connect()
    console.log('readSqueaks, username: ' + username)

    let res = await db.collection('squeaks').find({ $or: [{ recipient: username }, { recipient: 'all' }] })
        //.then(coll => console.log(coll))
        .toArray() // more performant to use next or foreach
        //        .then(coll => console.log('dbs squeaks : ' + JSON.stringify(coll)))
        .catch((error) => {
            console.log(error);
        });
    //    console.log(res)
    return res
}


exports.writeSqueak = async function(squeak) {
    if (db == null)
        await connect()
    db.collection('squeaks').insertOne(squeak)
        .then((result) => {
            // process result
            //console.log('written to db, result: ' + result)
        })
}

/*node
const dbService = require('./db-service');
dbService.migrateSqueaks();*/
exports.migrateSqueaks = async function() {
    if (db == null)
        await connect()
    let squeaks = fs.readFileSync('squeaks.json').toString()
    squeaks = JSON.parse(squeaks)
    let res = await db.collection('squeaks').insertMany(squeaks)
    console.log(res)
}

async function destroyDB() {
    if (db == null)
        await connect()
    db.collection('sessions').drop()
    //db.sessions.drop()
}

//destroyDB()

//migrateSqueaks()
//writeDB()

// readDB()
//     .then(squeaks => console.log('read from db: ' + squeaks))

//let squeaks = readDB()
//console.log(squeaks)

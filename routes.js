const express = require('express');
const router = express.Router()
const contr = require('./controller');

console.log('routes')

router.get('/', contr.root)
//router.get('/csrf', contr.csrf)
// router.get('/signin', contr.sign_in)
router.post('/signin', contr.sign_in)
router.post('/signup', contr.sign_up)
router.post('/signout', contr.sign_out)
router.post('/squeak', contr.squeak)
module.exports = router;
